package ru.t1.stepanischev.tm.exception;

public abstract class AbstractException extends RuntimeException {

    public AbstractException() {
    }

    public AbstractException(final String message){
        super(message);
    }

    public AbstractException(final String message, final Throwable cause) {
        super(message, cause);
    }

    public AbstractException(final Throwable cause) {
        super(cause);
    }

    public  AbstractException (final String message, final Throwable cause, final boolean enableSuppression, final boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

}