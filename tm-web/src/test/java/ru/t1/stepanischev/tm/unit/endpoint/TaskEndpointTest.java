package ru.t1.stepanischev.tm.unit.endpoint;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import ru.t1.stepanischev.tm.configuration.DataBaseConfiguration;
import ru.t1.stepanischev.tm.configuration.WebApplicationConfiguration;
import ru.t1.stepanischev.tm.marker.UnitCategory;
import ru.t1.stepanischev.tm.model.TaskDTO;

import java.util.Arrays;
import java.util.List;

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {WebApplicationConfiguration.class, DataBaseConfiguration.class})
@Category(UnitCategory.class)
public class TaskEndpointTest {

    @NotNull
    @Autowired
    private AuthenticationManager authenticationManager;

    @NotNull
    private MockMvc mockMvc;

    @NotNull
    @Autowired
    private WebApplicationContext wac;

    @NotNull
    final static String PROJECTS_URL = "http://localhost:8080/api/tasks/";

    @NotNull
    private final TaskDTO task1 = new TaskDTO("Test Task 1");

    @NotNull
    private final TaskDTO task2 = new TaskDTO("Test Task 2");

    @Before
    public void initTest() {
        mockMvc = MockMvcBuilders.webAppContextSetup(wac).build();
        @NotNull final UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken("kek", "kek");
        @NotNull final Authentication authentication = authenticationManager.authenticate(token);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        add(task1);
        add(task2);
    }

    @After
    @SneakyThrows
    public void clean() {
        mockMvc.perform(MockMvcRequestBuilders.delete(PROJECTS_URL + "deleteAll")
                        .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @SneakyThrows
    private void add(@NotNull final TaskDTO task) {
        @NotNull String url = PROJECTS_URL + "save";
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final String json = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(task);
        mockMvc.perform(MockMvcRequestBuilders.post(url).content(json)
                        .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @SneakyThrows
    private List<TaskDTO> findAll() {
        @NotNull String url = PROJECTS_URL + "findAll";
        @NotNull final String json = mockMvc.perform(MockMvcRequestBuilders.get(url)
                        .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        return Arrays.asList(objectMapper.readValue(json, TaskDTO[].class));
    }

    @Test
    public void findAllTest() {
        Assert.assertEquals(2, findAll().size());
    }

    @SneakyThrows
    private TaskDTO findById(@NotNull final String id) {
        @NotNull String url = PROJECTS_URL + "findById/" + id;
        @NotNull final String json = mockMvc.perform(MockMvcRequestBuilders.get(url)
                        .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();
        if (json.equals("")) return null;
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.readValue(json, TaskDTO.class);
    }

    @Test
    public void findByIdTest() {
        add(task1);
        final TaskDTO task = findById(task1.getId());
        Assert.assertNotNull(task);
        Assert.assertEquals(task.getId(), task1.getId());
    }

    @Test
    @SneakyThrows
    public void deleteTest() {
        @NotNull String url = PROJECTS_URL + "delete";
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final String json = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(task2);
        mockMvc.perform(MockMvcRequestBuilders.post(url).content(json)
                        .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());
        Assert.assertNull(findById(task2.getId()));
    }

    @Test
    @SneakyThrows
    public void deleteByIdTest() {
        @NotNull String url = PROJECTS_URL + "deleteById/" + task1.getId();
        mockMvc.perform(MockMvcRequestBuilders.post(url)
                        .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());
        Assert.assertNull(findById(task1.getId()));
    }

    @Test
    public void saveTest() {
        add(task2);
        @Nullable final TaskDTO task = findById(task2.getId());
        Assert.assertNotNull(task);
        Assert.assertEquals(task2.getId(), task.getId());
    }

}