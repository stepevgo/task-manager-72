package ru.t1.stepanischev.tm.integration;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import org.springframework.http.*;
import org.springframework.web.client.RestTemplate;
import ru.t1.stepanischev.tm.marker.IntegrationCategory;
import ru.t1.stepanischev.tm.model.ProjectDTO;
import ru.t1.stepanischev.tm.model.Result;

import java.net.HttpCookie;
import java.util.Arrays;
import java.util.List;

@Category(IntegrationCategory.class)
public class ProjectRestEndpointTest {

    @Nullable
    private static String sessionId;

    @NotNull
    private static final String PROJECT_URL = "http://localhost:8080/api/projects/";

    @NotNull
    private final ProjectDTO project1 = new ProjectDTO("Test Project 1");

    @NotNull
    private final ProjectDTO project2 = new ProjectDTO("Test Project 2");

    @NotNull
    private final ProjectDTO project3 = new ProjectDTO("Test Project 3");

    @NotNull
    private static final HttpHeaders header = new HttpHeaders();

    @BeforeClass
    public static void beforeClass() {
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        @NotNull final String url = "http://localhost:8080/api/auth/login?username=kek&password=kek";
        @NotNull final ResponseEntity<Result> response = restTemplate.postForEntity(url, Result.class, Result.class);
        System.out.println(response);
        Assert.assertEquals(200, response.getStatusCodeValue());
        Assert.assertNotNull(response.getBody());
        Assert.assertTrue(response.getBody().getSuccess());
        @NotNull final HttpHeaders headersResponse = response.getHeaders();
        @NotNull final List<HttpCookie> cookies = HttpCookie.parse(
                headersResponse.getFirst(HttpHeaders.SET_COOKIE)
        );
        sessionId = cookies.stream()
                .filter(item -> "JSESSIONID".equals(item.getName()))
                .findFirst().get().getValue();
        Assert.assertNotNull(sessionId);
        header.put(HttpHeaders.COOKIE, Arrays.asList("JSESSIONID=" + sessionId));
        header.setContentType(MediaType.APPLICATION_JSON);
    }

    @AfterClass
    public static void logout() {
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        @NotNull final String logoutUrl = "http://localhost:8080/api/auth/logout";
        sendRequest(logoutUrl, HttpMethod.POST, new HttpEntity<>(header));
    }

    private static ResponseEntity<ProjectDTO> sendRequest(
            @NotNull final String url,
            @NotNull final HttpMethod method,
            @NotNull final HttpEntity httpEntity
    ) {
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        return restTemplate.exchange(url, method, httpEntity, ProjectDTO.class);
    }

    @Before
    public void initTest() {
        @NotNull final String url = PROJECT_URL + "save/";
        sendRequest(url, HttpMethod.POST, new HttpEntity<>(project1, header));
        sendRequest(url, HttpMethod.POST, new HttpEntity<>(project2, header));
    }

    @After
    public void clean() {
        @NotNull final String url = PROJECT_URL + "deleteAll/";
        sendRequest(url, HttpMethod.POST, new HttpEntity<>(header));
    }

    @Test
    public void saveTest() {
        @NotNull final String expected = project3.getName();
        @NotNull final String url = PROJECT_URL + "save/";
        @NotNull final ResponseEntity<ProjectDTO> response = sendRequest(url, HttpMethod.POST, new HttpEntity<>(project3, header));
        Assert.assertEquals(response.getStatusCode(), (HttpStatus.OK));
        @Nullable final ProjectDTO project = response.getBody();
        Assert.assertNotNull(project);
        @NotNull final String actual = project.getName();
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void findByIdTest() {
        @NotNull final String id = project1.getId();
        @NotNull final String url = PROJECT_URL + "findById/" + id;
        @NotNull final ResponseEntity<ProjectDTO> response = sendRequest(url, HttpMethod.GET, new HttpEntity<>(header));
        Assert.assertEquals(response.getStatusCode(), (HttpStatus.OK));
        @Nullable final ProjectDTO project = response.getBody();
        Assert.assertNotNull(project);
        final String actual = project.getId();
        Assert.assertEquals(id, actual);
    }

    @Test
    public void deleteByIdTest() {
        @NotNull final String id = project1.getId();
        @NotNull final String url = PROJECT_URL + "deleteById/" + id;
        sendRequest(url, HttpMethod.POST, new HttpEntity<>(header));
        @NotNull final String urlFind = PROJECT_URL + "findById/" + id;
        Assert.assertNull(sendRequest(urlFind, HttpMethod.GET, new HttpEntity<>(header)).getBody());
    }

}
