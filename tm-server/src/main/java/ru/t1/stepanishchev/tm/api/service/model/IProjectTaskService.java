package ru.t1.stepanishchev.tm.api.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Service;
import ru.t1.stepanishchev.tm.model.Project;

@Service
public interface IProjectTaskService {

    void bindTaskToProject(@Nullable String userId, @Nullable String projectId, @Nullable String taskId);

    void unbindTaskFromProject(@Nullable String userId, @Nullable String projectId, @Nullable String taskId);

    Project removeProjectById(@Nullable String userId, @NotNull String projectId);

}