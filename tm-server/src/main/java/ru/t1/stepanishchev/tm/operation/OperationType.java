package ru.t1.stepanishchev.tm.operation;

public enum OperationType {

    INSERT,
    UPDATE,
    DELETE;

}