package ru.t1.stepanishchev.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.Scope;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.t1.stepanishchev.tm.dto.model.AbstractModelDTO;

import java.util.List;

@Repository
public interface AbstractDTORepository<M extends AbstractModelDTO> extends JpaRepository<M, String> {

    long count();

    void delete(@NotNull final M model);

    void deleteAll();

    void deleteById(@NotNull final String id);

    boolean existsById(@NotNull final String id);

    @NotNull
    List<M> findAll();

}
