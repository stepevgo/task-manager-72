package ru.t1.stepanishchev.tm.dto.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.stepanishchev.tm.enumerated.Status;

import javax.persistence.Column;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.MappedSuperclass;
import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
@MappedSuperclass
public abstract class AbstractUserOwnedModelDTO extends AbstractModelDTO {

    @NotNull
    @Column(nullable = false, name = "user_id")
    private String userId;

    @Nullable
    @Column(nullable = true, name = "created")
    private Date created = new Date();

    public AbstractUserOwnedModelDTO(@Nullable String userId) {
        this.userId = userId;
    }

}